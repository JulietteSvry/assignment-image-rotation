#include "image.h"

#include <malloc.h>
#include <stdint.h>

struct image create_img(struct image const *image) {
   struct image img = { 
  .width = image->height,
  .height = image->width,
  .data = malloc(sizeof(struct pixel) * image->width * image->height)};  
  return img;
}

struct image rotate(struct image const *image) {
  struct image newImg = create_img(image);
  for (size_t i = 0; i < image->height; ++i) {
	for (size_t j = 0; j < image->width; ++j) {
	newImg.data[image->height * j + (image->height - 1 - i)] =
          	image->data[image->width * i + j];
	}
  }  
  return newImg;
}
