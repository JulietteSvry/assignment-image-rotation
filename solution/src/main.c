#include "../include/bmp_run.h"
#include "../include/image.h"

#define SUCCESS 0 
#define FAILURE 1

int main(int argc, char **argv) { //(void) argc; (void) argv; suppression of
  (void)argc;
  (void)argv;                                 //'unused parameters' warning
  if (argc != 3) {
     fprintf(stderr,"Wrong number of arguments\n");
     return FAILURE;
  }
  
  FILE* input_bmp = fopen(argv[1], "rb");
  struct image input_image = {0};
  if (from_bmp(input_bmp, &input_image) != READ_OK) {
    fprintf(stderr,"Unable to open file for reading\n");
    return FAILURE;
  }
  fclose(input_bmp);
  
  FILE* output_bmp = fopen(argv[2], "wb");
  struct image output_image = rotate(&input_image);
  if (to_bmp(output_bmp, &output_image) != WRITE_OK) {
    fprintf(stderr, "Unable to open file for writing\n");
    return FAILURE;
  }
  fclose(output_bmp);
  
  free_memory(&input_image);
  free_memory(&output_image);
  return SUCCESS;
}
