#include "../include/bmp_run.h"
#include "image.h"

#include <stdlib.h>

#define DWORD 4
#define COLOR_DEPTH 24
#define HEADER_SIZE 40
#define RESERVED 0
#define TYPE 0x4D42
#define PLANES 1

#pragma pack(push, 1)
struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

uint8_t get_padd(struct image const *img) {
  if (img->width % DWORD) {
    return DWORD - ((img->width * sizeof(struct pixel)) % DWORD);
  }
  return 0;
}

struct bmp_header new_header( const struct image *img ) {
    struct bmp_header header = {
            .bfType =  TYPE,
            .bfileSize = sizeof(struct bmp_header) + get_padd(img) * img->height,
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = HEADER_SIZE,
            .biWidth = img -> width,
            .biHeight = img -> height,
            .biPlanes = PLANES,
            .biBitCount = COLOR_DEPTH,
            .biCompression = 0,
            .biSizeImage = img->height * (img->width + get_padd(img)),
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };
    return header;
}

static enum read_status check_bmp_header(FILE* into, struct bmp_header* header) {
  if (!into) {
    return READ_NO_FILE;
  }
  if (fread(header, sizeof(struct bmp_header), 1, into) != 1) {
        return READ_ERROR;
  }
  if (header->bfType != TYPE) {
    return READ_INVALID_SIGNATURE;
  }
  return READ_OK;
}

static enum read_status img_from_header(struct image *img, struct bmp_header* header, FILE *into) {
  img->width = header->biWidth;
  img->height = header->biHeight;
  const uint32_t heigth = img->height;
  const uint32_t width = img->width;
  const uint8_t padding = get_padd(img);
  struct pixel *data = malloc(width * heigth * sizeof(struct pixel));
  if (!data) {
  	fprintf(stderr, "Failed to allocate memory\n");
  	return READ_ERROR;
  }
  size_t index = 0;
  do {
    if (!fread(&(data[index * width]), sizeof(struct pixel), width, into) ||
        fseek(into, padding, SEEK_CUR) != 0) {
      return READ_ERROR;
    }
    ++index;
  } while (index < heigth);
  img->data = data;
  return READ_OK;
}

enum read_status from_bmp(FILE *into, struct image *img) {
  struct bmp_header header = {0};
  enum read_status status = check_bmp_header(into, &header);
  if (status != READ_OK) {
    return status;
  }
  
  status = img_from_header(img, &header, into);
  if (status != READ_OK) {
    return status;
  }
  return READ_OK;
}

enum write_status to_bmp(FILE *out, struct image const *img) {
  const struct bmp_header header = new_header(img);
  if ((fwrite(&header, sizeof(struct bmp_header), 1, out)) != 1) {
  	return WRITE_ERROR;
  }
  const uint32_t heigth = img->height;
  const uint32_t width = img->width;
  const uint8_t padding = get_padd(img);
  const uint32_t padd = 0;
  size_t index = 0;
  do {
    if (!fwrite(&img->data[index * width], sizeof(struct pixel), width, out) ||
        !fwrite(&padd, 1, padding, out)) {
      return WRITE_ERROR;
    }
    ++index;
  } while (index < heigth);
  return WRITE_OK;
}

void free_memory(struct image* img) {
	free(img->data);
	img->data = NULL;
}
