#ifndef SOLUTION_BMP_RUN_H
#define SOLUTION_BMP_RUN_H

#include "image.h"

#include <stdbool.h>
#include <stdio.h>

enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_NO_FILE,
    READ_ERROR
};

enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum read_status from_bmp(FILE *into, struct image *img);

enum write_status to_bmp(FILE *output, struct image const *img);

#endif // SOLUTION_BMP_RUN_H
